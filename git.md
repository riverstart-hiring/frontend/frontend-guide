# GIT

## Общие рекомендации

- Название коммита не должно превышать 50 символов 
- Название коммита должно начинаться с большой буквы
- Не нужно ставить знаки препинания в конце названия коммита
- Используй повелительное наклонение в названии коммита (в неопределенной форме)
  - Многие используют баг-трекер. Задача в баг-трекере должна отвечать на вопрос «Что сделать?». К примеру:
    - Название задачи: «Написать скрипт работы с почтой»
    - Описание: «Написать скрипт для работы с почтой. Скрипт должен принимать на вход текст письма, а на выход отдавать результат отправки письма.»
  - Правильный коммит должен правильно завершать фразу:
    - If applied, this commit will your subject line here 
  - Правильно:
    - If applied, this commit will refactor subsystem X for readability 
    - If applied, this commit will update getting started documentation 
  - Не правильно:
    - If applied, this commit will fixed bug with Y 
    - If applied, this commit will changing behavior of X
- По максимуму разбивайте комиты на отдельные выполненные блоки. 
  - Правильно:
    - ```
      58c00ee Refactor some block
      47145d0 Update javascript and mobile layout
      b98fe3c Create footer and main sections
      775f972 Create header section
      5250703 Initiate project
      ```
  - Не правильно:
    - ```
      58c00ee Create full page
      ```