# CSS

## Общие правила

Согласовываем с менеджером, что мы хотим получить на выходе - один общий файл стилей или несколько для разных разделов сайта.

Фреймворки используем только при необходимости. Если используем, то последней стабильной версии. Не нужно для одностраничного сайта тянуть bootstrap. Если все же решили использовать, то подключаем только то, что действительно нужно.

Используем полный ресет стилей браузера (см. файл _custom-reset.scss в заготовке или из используемого css-фреймворка).

Для всех кнопок и инпутов, которые используются как кнопки, не забываем прописать `cursor: pointer`.

Верстаем mobile-first. Тема холиварная, но вроде как с точки зрения производительности так правильнее.

Не забываем отслеживать переходы между breakpoints. Т.е. если мобильное меню было открыто и пользователь увеличил окно, меню должно корректно закрыться, кнопки активации меню должны вернуться в исходное состояние.

**Никогда** не навешиваем стили на html-теги. Недопустимо: `ul li a span { color: black; }` Разработчику может понадобиться добавить еще один span внутри a.

В тоже время, для контентных блоков, которые будут заполняться клиентом через текстовый редактор системы управления контентом, стили навешиваем непосредственно на html-теги.

Не используем #id для стилизации.

Для именования классов используем методологию [БЭМ](https://ru.bem.info/methodology/quick-start/) с допущениями.  Модификаторы для элемента пишем без перечня названий блоков-элементов, т.е. `product-list__item _popular`, а не `product-list__item product-list__item_popular`. Модификатор располагаем непосредственно после названия класса элемента, к которому он относится. Модификатор без этого класса никак не должен влиять на элемент.

Никакого транскрипта в названиях классов (`kartochka-tovara`), логически понятные английские названия (`product-card`).

Если используем background-image, то проверяем, умеет ли работать с такими изображениями используемый js-плагин «ленивой» загрузки. Если не работает, используем другие варианты. Помним, что изображения, жестко прописанные в стилях, сложнее заменить через систему управления контентом, не создаем дополнительных проблем разработчику.

Табличную верстку применяем только для таблиц.

Цвет фона для body всегда прописываем, даже если он белый.

Ресайз textarea (если он не отключен) не должен ломать вёрстку.

Проверяем блоки на «переполнение» текстом. Если в макете нарисована идеальная ситуция, когда все названия товаров занимают в листинге ровно две строки, то это не наш случай. В реальной жизни такого не бывает. Независимо от количества текста, карточки товаров должны быть одинаковой высоты. Все непонятные моменты согласовываем с дизайнером/менеджером.

Все анимации, какие только можно, реализуем через css3 transition.

Для всех ссылок/кнопок и других активных элементов предусматриваем состояния `:hover`, `:active` и `:focus`, а также реакцию на `:visited`. При необходимости требуем от дизайнера предоставить макет этих состояний.

Для иконок используем спрайты или иконочный шрифт.

## Препроцессоры

Используем препроцессоры ([scss](https://sass-scss.ru/documentation/)).

Все переменные выносим в отдельный файл. Стараемся максимально использовать переменные для простоты изменения макетов. 

Часто используемые конструкции (колонки, media-queries) выносим в mixins.

В отдельный файл выносим базовые стили для html, body и контейнера, если он есть. Сюда же пишем универсальные классы, которые могут пригодится разработчику/контентщику (hidden, nowrap, text-center и пр.). Предусматриваем классы для body, которые позволят зафиксировать скролл при открытии модальных элементов. Помним про эпл.

Стили для каждого логического блока описываем в отдельном файле и подключаем в конечный файл. Файлы называем по названию блока (`product-card.scss`), при необходимости пишем комментарии в начале файла и описываем, что это за блок и где используется.

Для вендорных префиксов используем [gulp-autoprefixer](https://www.npmjs.com/package/gulp-autoprefixer). Cписок поддерживаемых браузеров согласовываем с менеджером и прописываем в `package.json`, используем [browserslist](https://www.npmjs.com/package/browserslist).

## Шрифты

Локальные шрифты складываем в fonts и подключаем через @import в файле fonts.scss. Внешние подключаем с помощью [webfontloader](https://github.com/typekit/webfontloader). Подключаем только нужные семейства и стили шрифтов для минификации трафика. Если необходимый шрифт есть в свободном доступе на [Google Fonts](https://fonts.google.com/) – используем Google Fonts.

Не забываем прописывать базовые font-family `'My Cool Font', sans-serif`. Проверяем верстку «на прочность» при отсутствии шрифтов.

Прописываем `font-display: swap` для `@font-face`.

Шрифты с иконками собираем с помощью [fontello](http://fontello.com) и складываем также в fonts (сохраняя всю структуру архива с демо и конфигом). 

Названия шрифтов выносим в variables, в том числе и fontello.

## Дополнительно по согласованию с менеджером

- Версия для печати